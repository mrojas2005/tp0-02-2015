package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Map;

interface Operation {
	double run(double a, double b);
}

public class Operations {

	Map<String, Operation> operations = new HashMap<String, Operation>();

	public Operations() {
		
		this.operations.put( "+", (a, b) -> a + b);
		this.operations.put( "-", (a, b) -> a - b);
		this.operations.put( "*", (a, b) -> a * b);
		this.operations.put( "/", (a, b) -> a / b);
		this.operations.put( "%", (a, b) -> a % b);
	}

	public double rpn(String token, double args[]) {
		
		token = token.charAt(0) + "";
		double operand1, operand2;
		double result = args[0];
		
		for (int i = 1; i < args.length; i++) {				
			operand1 = result;
			operand2 = args[i];			
			result = this.operations.get(token).run(operand1, operand2);
		}
		
		return result;
	}

}
