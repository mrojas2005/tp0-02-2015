package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class RPNCalculator {
	
	public static void main(String[] args) {		
	}
	
	public double eval(String formula) throws IllegalArgumentException {

		if (isNullOrEmpty(formula))
			throw new IllegalArgumentException();

		formula = clean(formula);

		Operations operations = new Operations();		

		Stack<Double> stack = new Stack<Double>();

		int cant = 0;

		for (String token : formula.split("\\s")) {
			if (isNumber(token)) 
			{
				stack.push(Double.parseDouble(token));
				cant += 1;
			} 
			else if (isBinary(token) && cant > 1) 
			{
				double[] args = new double[2];
				args[1] = stack.pop();
				args[0] = stack.pop();
				stack.push(operations.rpn(token, args));
				cant -= 1;
			} 
			else if (isMultiple(token) && cant > 0) 
			{
				int n = cant;
				double[] args = new double[n];
				for (int i = n - 1; i >= 0; i--) {
					args[i] = stack.pop();
				}
				stack.push(operations.rpn(token, args));
				cant -= n - 1;
			} 
			else 
			{
				throw new IllegalArgumentException();
			}
		}
		
		return stack.pop();
	}

	// check string null or empty
	private static boolean isNullOrEmpty(String s) {
		return s == null || s.isEmpty();
	}

	// delete extra spaces and replace MOD operator
	private static String clean(String s) {
		return s.trim().replaceAll("MOD", "%").replaceAll(" +", " ");
	}

	// check token is number (integer or decimal)
	private static boolean isNumber(String s) {
		return s.matches("[-]?[0-9]*\\.?[0-9]+");
	}

	// check string is valid binary operation	
	private static boolean isBinary(String s) {
		return s.matches("[\\*|\\/|\\%|\\+|\\-]");
	}

	// check string is valid multiple operation
	private static boolean isMultiple(String s) {
		return s.matches("[\\*|\\/|\\%|\\+|\\-]{2}");
	}
	
}